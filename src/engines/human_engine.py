import chess.engine

from engines.engine import Engine, ScoredMove

class HumanEngine(Engine):
    """Chess engine using the abilities of a human player"""
    """List of keywords to abort input"""
    ABORT_KEYWORDS = ["EXIT", "CANCEL"]
    DEBUG_KEYWORDS = ["FEN", "DEBUG"]

    def play(self, board: chess.Board) -> chess.engine.PlayResult:
        """Play method taking input of human players"""
        whitesTurn = board.turn is chess.WHITE
        legalMoves = board.legal_moves
        while True:
            enteredMove = input(
                f"Enter move for {'white' if whitesTurn else 'black'} in UCI notation: "
            )
            if enteredMove.upper() in HumanEngine.ABORT_KEYWORDS:
                raise Exception("User aborted")

            print(enteredMove)

            if enteredMove.upper() in HumanEngine.DEBUG_KEYWORDS:
                print(
                    f"""
                    FEN: {board.fen()}
                    """
                )
            else:
                try:
                    move = chess.Move.from_uci(enteredMove)
                    if move in legalMoves:
                        return ScoredMove(0, move)
                    print("Illegal input. Please try again.")
                except ValueError:
                    print(
                        f"Illegal notation. Valid moves are {list(legalMoves)}"
                    )

    def analyse(self, board: chess.Board) -> list[ScoredMove]:
        return [ScoredMove(0, move) for move in board.legal_moves]