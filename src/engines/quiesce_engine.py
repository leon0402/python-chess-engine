import random

import chess.engine

from engines.engine import Engine, ScoredMove
from evaluation.evaluator import IncrementalEvaluator
from engines.cache import AlphaBetaCache, cache_alpha_beta, NodeType

class QuiesceEngine(Engine):
    PLAYER_MULTIPLIER = { chess.WHITE: 1, chess.BLACK: -1}
    
    def __init__(self, evaluator: IncrementalEvaluator, max_look_ahead_depth):
        self.evaluator = evaluator
        self.max_look_ahead_depth = max_look_ahead_depth

    def _evaluate_moves(self, board: chess.Board):
        print(f"Max depth: {self.max_look_ahead_depth}")

        for depth in range(self.max_look_ahead_depth + 1):
            scoredMoves = [
                self._evaluate_move(board, move, depth)
                for move in board.legal_moves
            ]

            print(f"Depth {depth}")
            # print(f"result: {scoredMoves}\n")

        return scoredMoves

    def _evaluate_move(self, board: chess.Board, move: chess.Move, depth: int):
        board.push(move)
        self.evaluator.init(board)
        score = self._value(
            board,
            depth,
            -self.evaluator.value_checkmate,
            self.evaluator.value_checkmate
        )
        score *= self.PLAYER_MULTIPLIER[board.turn]
        board.pop()
        return ScoredMove(score=score, move=move)

    def _value(self, board: chess.Board, depth: int, alpha: int, beta: int) -> int:
        if score := self.evaluator.evaluate(board):
            return score
        if depth == 0:
            return self._quiesce(board, alpha, beta)

        for move in board.legal_moves:
            movingPiece = board.piece_at(move.from_square)
            en_passent = board.is_en_passant(move)
            if en_passent:
                capturedPiece = chess.Piece(chess.PAWN, not board.turn)
            else:
                capturedPiece = board.piece_at(move.to_square)

            self.evaluator.push(movingPiece, capturedPiece, en_passent, move)
            board.push(move)
            value = -1 * self._value(board, depth - 1, -beta, -alpha)
            self.evaluator.pop()
            board.pop()

            if value >= beta:
                return value
            alpha = max(alpha, value)

        return alpha

    def _quiesce(self, board: chess.Board, alpha: int, beta: int) -> int:
        stand_pat = self.evaluator.getScore()

        if stand_pat >= beta:
            return beta
        alpha = max(alpha, stand_pat)

        for move in board.generate_legal_captures():
            caputuringPiece = board.piece_at(move.from_square)
            en_passent = board.is_en_passant(move)
            if en_passent:
                capturedPiece = chess.Piece(chess.PAWN, not board.turn)
            else:
                capturedPiece = board.piece_at(move.to_square)

            # Cutoff only if no promotion is involved
            if move.promotion is None:
                # Delta cutoff
                # TODO: Don't do this in endgames
                if stand_pat + self.evaluator.piece_values[
                    capturedPiece.piece_type] + 200 < alpha:
                    continue

                # if self._badCapture(caputuringPiece, capturedPiece):
                #     continue

            self.evaluator.push(
                caputuringPiece, capturedPiece, en_passent, move
            )
            board.push(move)

            # static exchange evaluation
            if self.evaluator.piece_values[capturedPiece.piece_type] - self.see(
                board, move.to_square
            ) < 0:
                self.evaluator.pop()
                board.pop()
                continue

            value = -1 * self._quiesce(board, -beta, -alpha)

            self.evaluator.pop()
            board.pop()

            if value >= beta:
                return value
            alpha = max(alpha, value)
        return alpha

    # Alternatives: MVV_LVA
    # https://stackoverflow.com/questions/37878665/quiescence-search-in-a-chess-computer
    def see(self, board: chess.Board, square: chess.Square):
        # TODO: Not checking for pins currently!
        attackers_squares = board.attackers(board.turn, square)
        if not attackers_squares:
            return 0

        attacker_square = min(
            attackers_squares,
            key=lambda attackers_square: board.piece_type_at(attackers_square)
        )
        capturedPiece = board.piece_type_at(square)

        board.push(chess.Move(from_square=attacker_square, to_square=square))
        value = self.evaluator.piece_values[capturedPiece] - self.see(
            board, square
        )
        board.pop()
        return value


    def play(self, board: chess.Board) -> chess.engine.PlayResult:
        bestScoredMove = self.analyse(board)[0]
        return chess.engine.PlayResult(move=bestScoredMove.move, ponder=None)

    def analyse(self, board: chess.Board) -> list[ScoredMove]:
        nextMoves = self._evaluate_moves(board.copy(stack=False))
        random.shuffle(nextMoves)

        whitesTurn = board.turn is chess.WHITE
        nextMoves.sort(reverse=whitesTurn)

        return nextMoves

class QuiesceCacheEngine(QuiesceEngine):
    def __init__(self,  evaluator: IncrementalEvaluator, max_look_ahead_depth: int):
        super().__init__(evaluator, max_look_ahead_depth)
        self.cache = AlphaBetaCache()

    def play(self, board: chess.Board) -> chess.engine.PlayResult:
        bestScoredMove = self._evaluate_moves2(board)
        print(f"Score: {bestScoredMove.score}")
        return chess.engine.PlayResult(move=bestScoredMove.move, ponder=None)

    def _evaluate_moves2(self, board: chess.Board) -> ScoredMove:
        print(f"Max depth: {self.max_look_ahead_depth}")
        self.cache.clear()
        self.evaluator.init(board)

        for depth in range(self.max_look_ahead_depth + 1):
            score = self._value(
                board,
                depth,
                -self.evaluator.value_checkmate,
                self.evaluator.value_checkmate
            )
            print(f"Depth {depth}")

        for move in board.legal_moves:
            board.push(move)
            cacheKey = self.cache.get_key(board)
            board.pop()

            try:
                type, value = self.cache.load_cache(cacheKey, depth - 1)

                if type == NodeType.EXACT and value == -score:
                    score *= self.PLAYER_MULTIPLIER[board.turn]
                    return ScoredMove(score=score, move=move)
            except:
                pass

    def _evaluate_moves(self, board: chess.Board):
        self.cache.clear()
        return super()._evaluate_moves(board)

    @cache_alpha_beta
    def _value(
        self, board: chess.Board, depth: int, alpha: int, beta: int
    ) -> int:
        return super()._value(board, depth, alpha, beta)