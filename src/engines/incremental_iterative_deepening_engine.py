import random

import chess.engine

from engines.engine import Engine, ScoredMove
from evaluation.evaluator import IncrementalEvaluator
from engines.cache import AlphaBetaCache, cache_alpha_beta

class IncrementalIterativeDeepeningEngine(Engine):
    PLAYER_MULTIPLIER = { chess.WHITE: 1, chess.BLACK: -1}
    
    def __init__(self, evaluator: IncrementalEvaluator, max_look_ahead_depth):
        self.evaluator = evaluator
        self.max_look_ahead_depth = max_look_ahead_depth

    def _evaluate_moves(self, board: chess.Board):
        print(f"Max depth: {self.max_look_ahead_depth}")

        for depth in range(self.max_look_ahead_depth + 1):
            scoredMoves = [
                self._evaluate_move(board, move, depth)
                for move in board.legal_moves
            ]

            print(f"Depth {depth}")
            # print(f"result: {scoredMoves}\n")

        return scoredMoves

    def _evaluate_move(self, board: chess.Board, move: chess.Move, depth: int):
        board.push(move)
        self.evaluator.init(board)
        score = self._value(
            board,
            depth,
            -self.evaluator.value_checkmate,
            self.evaluator.value_checkmate
        )
        score *= self.PLAYER_MULTIPLIER[board.turn]
        board.pop()
        return ScoredMove(score=score, move=move)

    def _value(self, board: chess.Board, depth: int, alpha: int, beta: int) -> int:
        if score := self.evaluator.evaluate(board):
            return score
        if depth == 0:
            return self.evaluator.heuristic(board)

        for move in board.legal_moves:
            movingPiece = board.piece_at(move.from_square)
            en_passent = board.is_en_passant(move)
            if en_passent:
                capturedPiece = chess.Piece(chess.PAWN, not board.turn)
            else:
                capturedPiece = board.piece_at(move.to_square)

            self.evaluator.push(movingPiece, capturedPiece, en_passent, move)
            board.push(move)
            value = -1 * self._value(board, depth - 1, -beta, -alpha)
            self.evaluator.pop()
            board.pop()

            if value >= beta:
                return value
            alpha = max(alpha, value)

        return alpha


    def play(self, board: chess.Board) -> chess.engine.PlayResult:
        bestScoredMove = self.analyse(board)[0]
        return chess.engine.PlayResult(move=bestScoredMove.move, ponder=None)

    def analyse(self, board: chess.Board) -> list[ScoredMove]:
        nextMoves = self._evaluate_moves(board.copy(stack=False))
        random.shuffle(nextMoves)

        whitesTurn = board.turn is chess.WHITE
        nextMoves.sort(reverse=whitesTurn)

        return nextMoves

class IncrementalIterativeDeepeningCacheEngine(IncrementalIterativeDeepeningEngine):
    def __init__(self,  evaluator: IncrementalEvaluator, max_look_ahead_depth: int):
        super().__init__(evaluator, max_look_ahead_depth)
        self.cache = AlphaBetaCache()

    def _evaluate_moves(self, board: chess.Board):
        self.cache.clear()
        return super()._evaluate_moves(board)

    @cache_alpha_beta
    def _value(
        self, board: chess.Board, depth: int, alpha: int, beta: int
    ) -> int:
        return super()._value(board, depth, alpha, beta)