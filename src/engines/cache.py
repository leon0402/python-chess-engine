from functools import wraps
from enum import Enum
from dataclasses import dataclass

import chess

class NodeType(Enum):
    EXACT = 0  # PV Node
    LOWER_BOUND = 1  # Cut Node
    UPPER_BOUND = 2  # All Node

@dataclass
class CachedPositionEntry:
    type: NodeType
    value: int
    depth: int

class AlphaBetaCache:
    def __init__(self):
        self.cache = dict()

    def clear(self):
        self.cache.clear()

    def size(self):
        return len(self.cache)

    def get_key(self, board: chess.Board):
        return board._transposition_key()

    def _get_node_type(self, value: int, depth: int, alpha: int, beta: int):
        # TODO: Correct in some cases, but not all (for instance with quiescene search, which does cutoffs)
        # if depth == 0:
        #     return NodeType.EXACT
        if value <= alpha:
            return NodeType.UPPER_BOUND
        if value >= beta:
            return NodeType.LOWER_BOUND
        return NodeType.EXACT

    def store_cache(self, key: int, value: int, depth: int, alpha: int, beta: int):
        # depth first replacement
        entry = self.cache.get(key)
        if entry and entry.depth >= depth:
            return

        node_type = self._get_node_type(value, depth, alpha, beta)
        self.cache[key] = CachedPositionEntry(node_type, value, depth)

    def load_cache(self, key: int, depth: int):
        entry = self.cache[key]
        if entry.depth < depth:
            raise KeyError

        return (entry.type, entry.value)

def cache_alpha_beta(value_function):

    @wraps(value_function)
    def cached_value(
        self, board: chess.Board, depth: int, alpha: int, beta: int
    ):
        cacheKey = self.cache.get_key(board)
        try:
            type, value = self.cache.load_cache(cacheKey, depth)

            if type == NodeType.EXACT:
                return value
            elif type == NodeType.LOWER_BOUND:
                alpha = max(value, alpha)
            else:  # type == NodeType.UPPER_BOUND
                beta = min(value, beta)

            if alpha >= beta:
                return value
        except:
            pass

        value = value_function(self, board, depth, alpha, beta)
        self.cache.store_cache(cacheKey, value, depth, alpha, beta)
        return value

    return cached_value