import random

import chess.engine

from engines.engine import Engine, ScoredMove
from evaluation.evaluator import Evaluator

class AlphaBetaPruningEngine(Engine):
    PLAYER_MULTIPLIER = { chess.WHITE: 1, chess.BLACK: -1}
    
    def __init__(self, evaluator: Evaluator, look_ahead_depth):
        self.evaluator = evaluator
        self.look_ahead_depth = look_ahead_depth

    def _evaluate_moves(self, board: chess.Board):
        return [self._evaluate_move(board, move) for move in board.legal_moves]

    def _evaluate_move(self, board: chess.Board, move: chess.Move):
        board.push(move)
        score = self._value(
            board,
            self.look_ahead_depth,
            -self.evaluator.value_checkmate,
            self.evaluator.value_checkmate
        )
        board.pop()
        return ScoredMove(score=score, move=move)

    def _value(self, board: chess.Board, depth: int, alpha: int, beta: int) -> int:
        if score := self.evaluator.evaluate(board):
            return self.PLAYER_MULTIPLIER[board.turn] * score
        if depth == 0:
            return self.PLAYER_MULTIPLIER[board.turn
                                        ] * self.evaluator.heuristic(board)

        if board.turn is chess.WHITE:
            for move in board.legal_moves:
                board.push(move)
                value = self._value(board, depth - 1, alpha, beta)
                board.pop()
                if value >= beta:
                    return value
                alpha = max(alpha, value)

            return alpha
        else:
            for move in board.legal_moves:
                board.push(move)
                value = self._value(board, depth - 1, alpha, beta)
                board.pop()
                if alpha >= value:
                    return value
                beta = min(beta, value)

            return beta


    def play(self, board: chess.Board) -> chess.engine.PlayResult:
        bestScoredMove = self.analyse(board)[0]
        return chess.engine.PlayResult(move=bestScoredMove.move, ponder=None)

    def analyse(self, board: chess.Board) -> list[ScoredMove]:
        nextMoves = self._evaluate_moves(board.copy(stack=False))
        random.shuffle(nextMoves)

        whitesTurn = board.turn is chess.WHITE
        nextMoves.sort(reverse=whitesTurn)

        return nextMoves