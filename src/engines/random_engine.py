import random

import chess.engine

from engines.engine import Engine, ScoredMove


class RandomEngine(Engine):
    """Basic chess engine playing a random legal move"""

    def play(self, board: chess.Board) -> chess.engine.PlayResult:
        move = random.choice(list(board.legal_moves))
        return chess.engine.PlayResult(move=move, ponder=None)

    def analyse(self, board: chess.Board) -> list[ScoredMove]:
        return [ScoredMove(0, move) for move in board.legal_moves]