from dataclasses import dataclass, field

import chess.engine
import chess


@dataclass(order=True)
class ScoredMove:
    """Class for representing a move along with a score."""
    score: int
    move: chess.Move = field(compare=False)


class Engine():
    """Common interface for all chess engines"""

    def play(self, board: chess.Board) -> chess.engine.PlayResult:
        pass

    def analyse(self, board: chess.Board) -> list[ScoredMove]:
        pass