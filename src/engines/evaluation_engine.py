import random

import chess.engine

from engines.engine import Engine, ScoredMove
from evaluation.evaluator import Evaluator

class EvaluationEngine(Engine): 
    PLAYER_MULTIPLIER = { chess.WHITE: 1, chess.BLACK: -1}

    def __init__(self, evaluator: Evaluator):
        self.evaluator = evaluator


    def play(self, board: chess.Board) -> chess.engine.PlayResult:
        bestScoredMove = self.analyse(board)[0]
        return chess.engine.PlayResult(move=bestScoredMove.move, ponder=None)

    def analyse(self, board: chess.Board) -> list[ScoredMove]:
        nextMoves = self._evaluate_moves(board.copy(stack=False))
        random.shuffle(nextMoves)

        whitesTurn = board.turn is chess.WHITE
        nextMoves.sort(reverse=whitesTurn)

        return nextMoves

    def _evaluate_moves(self, board: chess.Board):
        return [self._evaluate_move(board, move) for move in board.legal_moves]

    def _evaluate_move(self, board: chess.Board, move: chess.Move) -> ScoredMove:
        board.push(move)
        score = self.evaluator.evaluate(board)
        if score is None:
            score = self.evaluator.heuristic(board)
        score *= self.PLAYER_MULTIPLIER[board.turn]
        board.pop()

        return ScoredMove(score, move)