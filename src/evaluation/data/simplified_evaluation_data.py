import chess

VALUE_CHECKMATE = 30000
VALUE_DRAW = 0


PIECE_VALUES = {
    chess.PAWN: 100,
    chess.KNIGHT: 320,
    chess.BISHOP: 330,
    chess.ROOK: 500,
    chess.QUEEN: 900,
    chess.KING: 20000
}


def add_offset_to_square_values(square_values: list[list[int]], offset: int):
    for row in range(8):
        for column in range(8):
            square_values[row][column] += offset

PAWN_SQUARE_VALUES = [
      [ 0,  0,  0,  0,  0,  0,  0,  0],
      [50, 50, 50, 50, 50, 50, 50, 50],
      [10, 10, 20, 30, 30, 20, 10, 10],
      [ 5,  5, 10, 25, 25, 10,  5,  5],
      [ 0,  0,  0, 20, 20,  0,  0,  0],
      [ 5, -5,-10,  0,  0,-10, -5,  5],
      [ 5, 10, 10,-20,-20, 10, 10,  5],
      [ 0,  0,  0,  0,  0,  0,  0,  0]
] # yapf: disable

add_offset_to_square_values(PAWN_SQUARE_VALUES, PIECE_VALUES[chess.PAWN])

KNIGHT_SQUARE_VALUES = [
    [-50,-40,-30,-30,-30,-30,-40,-50],
    [-40,-20,  0,  0,  0,  0,-20,-40],
    [-30,  0, 10, 15, 15, 10,  0,-30],
    [-30,  5, 15, 20, 20, 15,  5,-30],
    [-30,  0, 15, 20, 20, 15,  0,-30],
    [-30,  5, 10, 15, 15, 10,  5,-30],
    [-40,-20,  0,  5,  5,  0,-20,-40],
    [-50,-40,-30,-30,-30,-30,-40,-50]
]  # yapf: disable

add_offset_to_square_values(KNIGHT_SQUARE_VALUES, PIECE_VALUES[chess.KNIGHT])


BISHOP_SQUARE_VALUES = [
    [-20,-10,-10,-10,-10,-10,-10,-20],
    [-10,  0,  0,  0,  0,  0,  0,-10],
    [-10,  0,  5, 10, 10,  5,  0,-10],
    [-10,  5,  5, 10, 10,  5,  5,-10],
    [-10,  0, 10, 10, 10, 10,  0,-10],
    [-10, 10, 10, 10, 10, 10, 10,-10],
    [-10,  5,  0,  0,  0,  0,  5,-10],
    [-20,-10,-10,-10,-10,-10,-10,-20]
]  # yapf: disable

add_offset_to_square_values(BISHOP_SQUARE_VALUES, PIECE_VALUES[chess.BISHOP])

ROOK_SQUARE_VALUES = [
    [ 0,  0,  0,  0,  0,  0,  0,  0],
    [ 5, 10, 10, 10, 10, 10, 10,  5],
    [-5,  0,  0,  0,  0,  0,  0, -5],
    [-5,  0,  0,  0,  0,  0,  0, -5],
    [-5,  0,  0,  0,  0,  0,  0, -5],
    [-5,  0,  0,  0,  0,  0,  0, -5],
    [-5,  0,  0,  0,  0,  0,  0, -5],
    [ 0,  0,  0,  5,  5,  0,  0,  0]
] # yapf: disable

add_offset_to_square_values(ROOK_SQUARE_VALUES, PIECE_VALUES[chess.ROOK])

QUEEN_SQUARE_VALUES = [
    [-20,-10,-10, -5, -5,-10,-10,-20],
    [-10,  0,  0,  0,  0,  0,  0,-10],
    [-10,  0,  5,  5,  5,  5,  0,-10],
    [ -5,  0,  5,  5,  5,  5,  0, -5],
    [  0,  0,  5,  5,  5,  5,  0, -5],
    [-10,  5,  5,  5,  5,  5,  0,-10],
    [-10,  0,  5,  0,  0,  0,  0,-10],
    [-20,-10,-10, -5, -5,-10,-10,-20]
] # yapf: disable

add_offset_to_square_values(QUEEN_SQUARE_VALUES, PIECE_VALUES[chess.QUEEN])

KING_SQUARE_VALUES = [
    [-30,-40,-40,-50,-50,-40,-40,-30],
    [-30,-40,-40,-50,-50,-40,-40,-30],
    [-30,-40,-40,-50,-50,-40,-40,-30],
    [-30,-40,-40,-50,-50,-40,-40,-30],
    [-20,-30,-30,-40,-40,-30,-30,-20],
    [-10,-20,-20,-20,-20,-20,-20,-10],
    [ 20, 20,  0,  0,  0,  0, 20, 20],
    [ 20, 30, 10,  0,  0, 10, 30, 20],
]# yapf: disable

add_offset_to_square_values(KING_SQUARE_VALUES, PIECE_VALUES[chess.KING])

PIECE_SQUARE_VALUES = {
    chess.PAWN: PAWN_SQUARE_VALUES,
    chess.KNIGHT: KNIGHT_SQUARE_VALUES,
    chess.BISHOP: BISHOP_SQUARE_VALUES,
    chess.ROOK: ROOK_SQUARE_VALUES,
    chess.QUEEN: QUEEN_SQUARE_VALUES,
    chess.KING: KING_SQUARE_VALUES
}