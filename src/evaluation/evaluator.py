import chess 

class Evaluator(): 
    def __init__(self, value_checkmate: int, value_draw: int):
        self.value_checkmate = value_checkmate
        self.value_draw = value_draw

    def heuristic(self, board: chess.Board) -> int:
        pass 

    def evaluate(self, board: chess.Board) -> int:
        if board.is_checkmate():
            return -self.value_checkmate
        if board.is_draw():
            return self.value_draw
        return None

class IncrementalEvaluator(Evaluator):
    def __init__(self, value_checkmate: int, value_draw: int): 
        super().__init__(value_checkmate, value_draw)

    def init(self, board: chess.Board):
        pass

    def getScore(self): 
        pass 

    def push(self, movedPiece: chess.Piece, capturedPiece: chess.Piece, enPassant: bool, move: chess.Move):
        pass

    def pop(self):
        pass