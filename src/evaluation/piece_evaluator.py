import chess

from evaluation.evaluator import Evaluator

def is_draw(self) -> bool:
    """Function to check for all any draw condition"""
    return self.is_stalemate() or self.is_insufficient_material(
    ) or self.is_fivefold_repetition() or self.is_seventyfive_moves()

chess.Board.is_draw = is_draw

class PieceEvaluator(Evaluator):
    def __init__(self, piece_values, value_checkmate: int, value_draw: int): 
        self.piece_values = piece_values
        self.value_checkmate = value_checkmate
        self.value_draw = value_draw

    def heuristic(self, board: chess.Board) -> int:
        """Calculate the value of all pieces on the board"""
        playerScore = 0
        for square in chess.scan_reversed(board.occupied_co[board.turn]):
            playerScore += self.piece_values[board.piece_type_at(square)]

        opponentScore = 0
        for square in chess.scan_reversed(board.occupied_co[not board.turn]):
            opponentScore += self.piece_values[board.piece_type_at(square)]

        return playerScore - opponentScore

    def evaluate(self, board: chess.Board) -> int:
        """Evaluate the current board"""
        if board.is_checkmate():
            return -self.value_checkmate
        if board.is_draw():
            return self.value_draw
        return None
