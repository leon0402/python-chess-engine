import chess

from evaluation.evaluator import Evaluator, IncrementalEvaluator

class PieceSquareEvaluator(Evaluator):
    def __init__(self, piece_values, piece_square_values, value_checkmate: int, value_draw: int): 
        super().__init__(value_checkmate, value_draw)
        self.piece_values = piece_values
        self.piece_square_values = piece_square_values

    def heuristic(self, board: chess.Board) -> int:
        """Calculate the value of all pieces on the board"""
        score = 0

        for square in chess.scan_reversed(board.occupied_co[board.turn]):
            score += self._get_square_value(board, square)

        for square in chess.scan_reversed(board.occupied_co[not board.turn]):
            score -= self._get_square_value(board, square)

        return score
    
    def _get_square_value(self, board: chess.Board, square: chess.Square):
        piece = board.piece_at(square)
        file = chess.square_file(square)
        rank = chess.square_rank(square)
        return self._get_piece_square_value(piece, file, rank)
    
    def _get_piece_square_value(self, piece: chess.Piece, file: int, rank: int):
        if piece.color == chess.WHITE:
            return self.piece_square_values[piece.piece_type][7 - rank][file]
        else:
            return self.piece_square_values[piece.piece_type][rank][file]

class IncrementalPieceSquareEvaluator(IncrementalEvaluator):
    def __init__(self, piece_values, piece_square_values,value_checkmate: int, value_draw: int): 
        super().__init__(value_checkmate, value_draw)
        self.piece_values = piece_values
        self.piece_square_values = piece_square_values
        self.scores = None

    def init(self, board: chess.Board):
        self.scores = [self.heuristic(board)]

    def push(self,movedPiece: chess.Piece, capturedPiece: chess.Piece, enPassant: bool, move: chess.Move):
        fromFile = chess.square_file(move.from_square)
        fromRank = chess.square_rank(move.from_square)
        toFile = chess.square_file(move.to_square)
        toRank = chess.square_rank(move.to_square)

        score = self.scores[-1]

        # Subtract old position of moving piece and add new position
        score -= self._get_piece_square_value(movedPiece, fromFile, fromRank)
        if move.promotion:
            movedPiece = chess.Piece(move.promotion, movedPiece.color)
        score += self._get_piece_square_value(movedPiece, toFile, toRank)

        # Add captured piece value if any
        if capturedPiece:
            score += self._get_piece_square_value(
                capturedPiece, fromFile if enPassant else toFile, toRank
            )

        self.scores.append(-1 * score)

    def getScore(self):
        return self.scores[-1]

    def pop(self):
        self.scores.pop()

    def heuristic(self, board: chess.Board) -> int:
        """Calculate the value of all pieces on the board"""
        score = 0

        for square in chess.scan_reversed(board.occupied_co[board.turn]):
            score += self._get_square_value(board, square)

        for square in chess.scan_reversed(board.occupied_co[not board.turn]):
            score -= self._get_square_value(board, square)

        return score
    
    def _get_square_value(self, board: chess.Board, square: chess.Square):
        piece = board.piece_at(square)
        file = chess.square_file(square)
        rank = chess.square_rank(square)
        return self._get_piece_square_value(piece, file, rank)
    
    def _get_piece_square_value(self, piece: chess.Piece, file: int, rank: int):
        if piece.color == chess.WHITE:
            return self.piece_square_values[piece.piece_type][7 - rank][file]
        else:
            return self.piece_square_values[piece.piece_type][rank][file]