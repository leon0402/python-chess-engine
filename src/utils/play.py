import time
from datetime import datetime

import IPython.display
import chess

import engines.engine



def displayChessBoard(board: chess.Board, display: bool = True):
    if display:
        IPython.display.clear_output(wait=True)
        IPython.display.display(board)
        time.sleep(0.5)


def logMove(
    board: chess.Board, move: chess.Move, fileName: str, log: bool = True
):
    if log:
        with open(fileName, "a") as f:
            if board.turn is chess.WHITE:
                f.write(board.lan(move) + "\t")
            else:
                f.write(board.lan(move) + "\n")


def playGame(
    board: chess.Board,
    engine1: engines.engine.Engine,
    engine2: engines.engine.Engine,
    displayBoard: bool = False,
    logMoves: bool = False
) -> chess.Board():
    """Function to play two Engines against each other
    
    Keyword arguments:
    displayBoard -- toggle to enable displaying the board after each half turn (default false)
    logMoves -- toggle to create a log of moves (default false)
    """
    engines = [engine1, engine2]

    logFileName = f"../log/{datetime.isoformat(datetime.now())}.txt"

    displayChessBoard(board, displayBoard)

    while not board.is_game_over():
        move = engines[0].play(board).move

        logMove(board, move, logFileName, logMoves)

        board.push(move)
        engines[0], engines[1] = engines[1], engines[0]

        displayChessBoard(board, displayBoard)
    return board